// Абстракция

abstract class AbstractPhone {
  private year: number;

  protected constructor(year: number) {
    this.year = year;
  }

  public call(outputNumber: number): void {

  };

  public abstract ring(inputNumber: number): void;
}

// Инкапсуляция
class SomePhone {
  private year: number;
  private company: string;

  constructor(year: number, company: string) {
    this.year = year;
    this.company = company;
  }

  private openConnection(): void {
    // findComutator
    // openNewConnection...
  }

  public call(): void {
    this.openConnection();
    console.log('Вызываю номер');
  }

  public ring(): void {
    console.log('Дзынь-дзынь');
  }
}

// Полиморфизм

// Полиморфизм подтипов

class User2 {
  private name: string;

  constructor(name: string) {
    this.name = name;
  }

  public callAnotherUser(num: number, tel: AbstractPhone): void {
    tel.call(num);
  }
}

class ThomasEdisonPhone extends AbstractPhone {

  constructor(year: number) {
    super(year);
  }

  public call(outputNumber: number): void {
    console.log('Вращайте ручку');
    console.log('Сообщите номер абонента, сэр');
  }

  public ring(inputNumber: number): void {
    console.log('Телефон звонит');
  }
}

class Phone extends AbstractPhone {

  constructor(year: number) {
    super(year);
  }

  public call(outputNumber: number): void {
    console.log('Вызываю номер' + outputNumber);
  }

  public ring(inputNumber: number): void {
    console.log('Телефон звонит');
  }
}

class VideoPhone extends AbstractPhone {

  constructor(year: number) {
    super(year);
  }

  public call(outputNumber: number): void {
    console.log('Подключаю видеоканал для абонента' + outputNumber);
  }

  public ring(inputNumber: number): void {
    console.log('У вас входящий видеовызов...' + inputNumber);
  }
}

const firstPhone: AbstractPhone = new ThomasEdisonPhone(1879);
const phone: AbstractPhone = new Phone(1984);
const videoPhone: AbstractPhone = new VideoPhone(2018);
const user: User2 = new User2('Андрей');

user.callAnotherUser(224466, firstPhone);
// Вращайте ручку
// Сообщите номер абонента, сэр
user.callAnotherUser(224466, phone);
// Вызываю номер 224466
user.callAnotherUser(224466, videoPhone);
// Подключаю видеоканал для абонента 224466
