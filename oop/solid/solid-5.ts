// Принцип подстановки Барбары Лисков

// Не правильно
class Rectangle4 {
  width: number;
  height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }

  setWidth(width: number): void {
    this.width = width;
  }

  setHeight(height: number): void {
    this.height = height;
  }

  areaOf(): number {
    return this.width * this.height;
  }
}

class Square extends Rectangle2 {
  width: number;
  height: number;

  constructor(size: number) {
    super(size, size);
  }

  setWidth(width: number): void {
    this.width = width;
    this.height = width;
  }

  setHeight(height: number): void {
    this.width = height;
    this.height = height;
  }
}

const square2: Square = new Square(2);

square2.setWidth(20); // меняет ширину и высоту, всё верно
square2.setHeight(40); // тоже меняет ширину и высоту, ок

function testShapeSize(figure: Rectangle4): void {
  figure.setWidth(10);
  figure.setHeight(20);
  console.log(figure.areaOf() === 200);
  // условие не сработает, если figure — экземпляр класса Square
}


// Правильно
abstract class RightAngleShape {
  // используется для изменения ширины или высоты,
  // доступен только внутри класса и наследников:
  protected setSide(size: number, side?: 'width' | 'height'): void {
  }

  abstract areaOf(): number;
}

class Square9 extends RightAngleShape {
  edge: number;

  constructor(size: number) {
    super();
    this.edge = size;
  }

  // переопределяем изменение стороны квадрата...
  protected setSide(size: number): void {
    this.edge = size;
  }

  setWidth(size: number): void {
    this.setSide(size);
  }

  // ...и вычисление площади
  areaOf(): number {
    return this.edge ** 2;
  }
}

class Rectangle8 extends RightAngleShape {
  width: number;
  height: number;

  constructor(width: number, height: number) {
    super();
    this.width = width;
    this.height = height;
  }

  // переопределяем изменение ширины и высоты...
  protected setSide(size: number, side: 'width' | 'height'): void {
    this[side] = size;
  }

  setWidth(size: number): void {
    this.setSide(size, 'width');
  }

  setHeight(size: number): void {
    this.setSide(size, 'height');
  }

  // ...и вычисление площади
  areaOf(): number {
    return this.width * this.height;
  }
}
