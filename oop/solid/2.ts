// Принцип открытости и закрытости
// Неправильно

class Rectangle {
  width: number;
  height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }
}

class AreaCalculator {
  shapes: Rectangle[];

  constructor(shapes: Rectangle[]) {
    this.shapes = shapes;
  }

  totalAreaOf(): number {
    return this.shapes.reduce((tally: number, shape: Rectangle) => {
      return tally += (shape.width * shape.height);
    }, 0);
  }
}

class Circle {
  radius: number;

  constructor(radius: number) {
    this.radius = radius;
  }
}

class AreaCalculator2 {
  // 1.ts. приходится менять тип
  shapes: [Rectangle | Circle];

  constructor(shapes: [Rectangle | Circle]) {
    this.shapes = shapes;
  }

  totalAreaOf(): number {
    return this.shapes.reduce((tally: number, shape: Rectangle | Circle) => {
      // 2. приходится проверять, какой тип,
      //    чтобы применить правильный расчёт
      if (shape instanceof Rectangle) {
        return tally += (shape.width * shape.height);
      } else if (shape instanceof Circle) {
        return tally += (shape.radius ** 2 * Math.PI);
      } else {
        return tally;
      }
    }, 0);
  }
}

// Правильно
interface AreaCalculatable {
  areaOf(): number;
}

// 1.ts. указываем, что класс реализует интерфейс,
//    это задаст ограничение на методы класса
class Rectangle2 implements AreaCalculatable {
  width: number;
  height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }

  // 2. без этого метода класс считается не готовым,
  //    он же позволит абстрагироваться от реализации конкретной фигуры
  areaOf(): number {
    return this.width * this.height;
  }
}

// те же изменения проводим для круга
class Circle2 implements AreaCalculatable {
  radius: number;

  constructor(radius: number) {
    this.radius = radius;
  }

  areaOf(): number {
    return Math.PI * (this.radius ** 2);
  }
}

