// Высокоуровневые модули не должны зависеть от низкоуровневых; оба типа должны зависеть от абстракций.
// Абстракции не должны зависеть от деталей, детали должны зависеть от абстракций.

// Не правильно

class MySqlConnection {/*...*/
}

class Auth {
  private connection: MySqlConnection;

  constructor(connection: MySqlConnection) {
    this.connection = connection;
  }

  async authentificate(login: string, password: string): Promise<any> {/*...*/
  }
}

// Правильно

















interface DataBaseConnection {
  connect(host: string, user: string, password: string): void;
}

class MySqlConnection2 implements DataBaseConnection {
  constructor() {/*...*/
  }

  connect(host: string, user: string, password: string): void {/*...*/
  }
}

class Auth2 {
  // тип зависимости поменялся:
  connection: DataBaseConnection;

  constructor(connection: DataBaseConnection) {
    this.connection = connection;
  }

  authentificate(login: string, password: string): void {/*...*/
  }
}
