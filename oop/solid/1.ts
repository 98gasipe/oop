// Принцип единой ответсвенности

// Неправильно
class ReportExporterBad {
  name: string;
  data: ReportData;

  constructor(name: string, data: ReportData) {
    this.name = name;
    this.data = data;
  }

  export(reportType: ReportTypes): string {
    if (reportType === ReportTypes.Html) {
      return this.formatHtml(null);
    }

    if (reportType === ReportTypes.Txt) {
      return this.formatTxT(null);
    }
  }

  formatTxT(data: ReportData): string {
    // форматируем данные в TXT и возвращаем:
    return 'txt string';
  }

  formatHtml(data: ReportData): string {
    // форматируем данные в HTML и возвращаем:
    return 'html string';
  }
}

const exporter = new ReportExporterBad('Prive', null);

// Правильно

// тип данных для отчёта
type ReportData = {
  content: string,
  date: Date,
  size: number,
};

// возможные форматы
enum ReportTypes {
  Html,
  Txt,
}

// класс, который занимается экспортом данных
class ReportExporter {
  name: string;
  data: ReportData;

  constructor(name: string, data: ReportData) {
    this.name = name;
    this.data = data;
  }

  export(reportType: ReportTypes): string {
    const formatter: Formatter = FormatSelector.selectFor(reportType);
    return formatter.format(this.data);
  }
}

interface Formatter {
  format(data: ReportData): string;
}

// класс для форматирования в HTML
class HtmlFormatter implements Formatter {
  format(data: ReportData): string {
    // форматируем данные в HTML и возвращаем:
    return 'html string';
  }
}

// класс для форматирования в TXT
class TxtFormatter implements Formatter {
  format(data: ReportData): string {
    // форматируем данные в TXT и возвращаем:
    return 'txt string';
  }
}

class FormatSelector {
  private static formatters = {
    [ReportTypes.Html]: HtmlFormatter,
    [ReportTypes.Txt]: TxtFormatter,
  };

  static selectFor(reportType: ReportTypes): Formatter {
    const FormatterFactory = FormatSelector.formatters[reportType];
    return new FormatterFactory();
  }
}

const dynamicFormatter = FormatSelector.selectFor(ReportTypes.Html);
dynamicFormatter.format(null);
