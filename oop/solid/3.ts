// Принцип разделения интерфейса

interface Programmer {
  writeCode(): void;

  eatPizza(slicesCount: number): void;
}

class RegularProgrammer implements Programmer {
  constructor() {/*...*/
  }

  writeCode(): void {/*...*/
  }

  eatPizza(slicesCount: number): void {/*...*/
  }
}

class Freelancer implements Programmer {
  constructor() {/*...*/
  }

  writeCode(): void {/*...*/
  }

  eatPizza(slicesCount: number): void {
    // здесь будет пусто
  }
}

// Решение
interface CodeProducer {
  writeCode(): void;
}

interface PizzaConsumer {
  eatPizza(slicesCount: number): void;
}

class RegularProgrammer2 implements CodeProducer, PizzaConsumer {
  constructor() {/*...*/
  }

  writeCode(): void {/*...*/
  }

  eatPizza(slicesCount: number): void {/*...*/
  }
}

class Freelancer2 implements CodeProducer {
  constructor() {/*...*/
  }

  writeCode(): void {/*...*/
  }

  // метод eatPizza уже не нужен
}
