// Наследование

class User {
  public id: number;
  public name: string;
}

class Manager extends User {
  public company: string;
}

// Ассоциация

// Например, игрок играет в определенной команде:

class Team {

}

class Player {
  public team: Team;

  constructor() {
    this.team = new Team();
  }
}

// Композиция

class ElectricEngine {
}

class Car {
  public engine: ElectricEngine;

  constructor() {
    this.engine = new ElectricEngine();
  }
}

// Агрегация

abstract class Engine {
}

class Car1 {
  public engine: Engine;

  constructor(eng: Engine) {
    this.engine = eng;
  }
}

// Реализация

interface IMovable {
  move(): void;
}

class Car2 implements IMovable {
  move(): void {
    console.log('Машина едет');
  }
}

